import './app.scss'
import Tetris from './tetris/Tetris'

function App() {
  return (
    <div className='App'>
      <Tetris />
    </div>
  )
}

export default App
