import React, { useRef, useState } from 'react'
import c from './tetris.module.scss'
import tetrominoes from './tetrominoes'

const Tetris = () => {
  const canvas = useRef()
  const next = useRef()

  const [score, setScore] = useState(0)

  const start = () => {
    // sizes

    const w = canvas.current.width
    const h = canvas.current.height
    const bs = w / 10

    // board

    const board = []

    for (let y = 0; y < 20; y++) {
      board.push([])

      for (let x = 0; x < 10; x++) {
        board[y].push({ occupied: false, coords: { x, y } })
      }
    }

    // piece

    const startPos = { x: 3, y: 0 }

    const generateNewPiece = () =>
      tetrominoes[Math.floor(Math.random() * tetrominoes.length)]

    const piece = {
      next: generateNewPiece(),
      current: generateNewPiece(),
      pos: { ...startPos },
      state: 0,
    }

    // progress piece

    const progressPiece = () => {
      piece.pos.y += 1
    }

    // move piece

    const movePiece = (direction) => {
      switch (direction) {
        case 'left':
          piece.pos.x -= 1
          break

        case 'right':
          piece.pos.x += 1
          break

        default:
          break
      }
    }

    // rotate piece

    const rotatePiece = () => {
      let nextState = piece.state + 1

      if (nextState > piece.current.rotation.length - 1) nextState = 0

      if (
        checkCollision('left', nextState, 'r') ||
        checkCollision('right', nextState, 'r')
      )
        nextState = piece.state

      piece.state = nextState
    }

    // set piece

    const setPiece = () => {
      piece.current.rotation[piece.state].forEach((block, i) => {
        const x = piece.pos.x + block.x
        const y = piece.pos.y + block.y

        board[y][x].occupied = true
        board[y][x].drawFn = piece.current.drawFn
        board[y][x].state = piece.state
        board[y][x].i = i
      })
    }

    // clear rows
    const clearRow = (row) => {
      board[row].forEach((block) => (block.occupied = false))

      for (let i = row - 1; i >= 0; i--) {
        board[i].forEach((block, j) => {
          board[i + 1][j].occupied = block.occupied
          board[i + 1][j].drawFn = block.drawFn
          board[i + 1][j].state = block.state
          board[i + 1][j].i = block.i
        })
      }
    }

    // check rows

    const checkRows = () => {
      let rows = 0

      board.forEach((row, i) => {
        if (row.every((block) => block.occupied)) {
          clearRow(i)
          rows++
        }
      })

      updateScore(rows)
    }

    // create new piece

    const createPiece = () => {
      piece.current = piece.next
      piece.next = generateNewPiece()
      piece.pos = { ...startPos }
      piece.state = 0
    }

    // collision check

    const checkCollision = (direction, state, type) => {
      let collision = false
      const t = type === 'r' ? 0 : 1

      // down
      switch (direction) {
        case 'down':
          piece.current.rotation[state].forEach((block) => {
            if (piece.pos.y + block.y + 1 >= h / bs) {
              collision = true
            } else if (
              board[piece.pos.y + block.y + 1][piece.pos.x + block.x].occupied
            ) {
              collision = true
            }
          })
          break

        // left
        case 'left':
          piece.current.rotation[state].forEach((block) => {
            if (piece.pos.x + block.x <= t - 1) {
              collision = true
            } else if (
              board[piece.pos.y + block.y][piece.pos.x + block.x - 1]?.occupied
            ) {
              collision = true
            }
          })
          break

        // right
        case 'right':
          piece.current.rotation[state].forEach((block) => {
            if (piece.pos.x + block.x + t >= w / bs) {
              collision = true
            } else if (
              board[piece.pos.y + block.y][piece.pos.x + block.x + 1]?.occupied
            ) {
              collision = true
            }
          })
          break

        default:
          break
      }

      return collision
    }

    // bind controls

    const bindControls = (e) => {
      switch (e.key) {
        case 'ArrowUp':
          rotatePiece()
          break

        case 'ArrowDown':
          if (!checkCollision('down', piece.state, 'm')) progressPiece()
          break

        case 'ArrowLeft':
          if (!checkCollision('left', piece.state, 'm')) movePiece('left')
          break

        case 'ArrowRight':
          if (!checkCollision('right', piece.state, 'm')) movePiece('right')
          break

        default:
          break
      }
    }

    window.addEventListener('keydown', bindControls)

    // draw

    const ctx = canvas.current.getContext('2d')

    const clear = () => {
      ctx.clearRect(0, 0, w, h)
      nextCtx.clearRect(0, 0, 200, 200)
    }

    const drawBoard = () => {
      const occupiedBoard = board.flat().filter((block) => block.occupied)

      occupiedBoard.forEach((block) => {
        block.drawFn(
          ctx,
          block.coords,
          { x: 0, y: 0 },
          bs,
          block.state,
          block.i
        )
      })
    }

    const drawPiece = () => {
      piece.current.rotation[piece.state].forEach((block, i) => {
        piece.current.drawFn(ctx, piece.pos, block, bs, piece.state, i)
      })

      // ctx.fillStyle = '#81b66a'
      // piece.current.rotation[piece.state].forEach((block) => {
      //   const x = (piece.pos.x + block.x) * bs
      //   const y = (piece.pos.y + block.y) * bs

      //   ctx.fillRect(x, y, bs, bs)
      // })
    }

    // draw next

    const nextCtx = next.current.getContext('2d')

    const drawNext = () => {
      piece.next.rotation[0].forEach((block, i) => {
        piece.next.drawFn(nextCtx, { x: 0, y: 0 }, block, bs, 0, i)
      })

      // nextCtx.fillStyle = '#81b66a'

      // piece.next.rotation[0].forEach((block) => {
      //   const x = block.x * bs
      //   const y = block.y * bs

      //   nextCtx.fillRect(x, y, bs, bs)
      // })
    }

    // update score

    const updateScore = (lines) => {
      if (lines) {
        setScore((f) => f + lines * lines * 100)
      }
    }

    // tick + tock

    const startTime = 60

    const time = {
      max: startTime,
      current: startTime,
    }

    const tick = () => {
      requestAnimationFrame(tick)

      time.current -= 1

      if (time.current === 0) tock()

      clear()
      drawBoard()
      drawPiece()
      drawNext()
      checkRows()
    }

    const tock = () => {
      time.current = time.max

      if (checkCollision('down', piece.state, 'm')) {
        setPiece()
        createPiece()
      } else {
        progressPiece()
      }
    }

    tick()
  }

  // render

  return (
    <div className={c.Tetris}>
      <div className={c.canvasContainer}>
        <div className={c.left}>
          <canvas width={400} height={800} className={c.canvas} ref={canvas} />
        </div>
        <div children={c.right}>
          <h1 className={c.score}>{score}</h1>
          <canvas width={200} height={200} className={c.next} ref={next} />
        </div>
      </div>
      <button onClick={start} className={c.start}>
        Start
      </button>
    </div>
  )
}

export default Tetris
