import React, { useRef, useState } from 'react'
import c from './tetris.module.scss'
import tetrominoes from './tetrominoes'

const Tetris = () => {
  const canvas = useRef()
  const next = useRef()

  const [score, setScore] = useState(0)

  const start = () => {
    // sizes

    const w = canvas.current.width
    const h = canvas.current.height
    const bs = w / 10

    // board

    const board = []

    for (let y = 0; y < 20; y++) {
      board.push([])

      for (let x = 0; x < 10; x++) {
        board[y].push({ occupied: false, coords: { x, y } })
      }
    }

    // piece

    const startPos = { x: 3, y: 0 }

    const generateNewPiece = () =>
      tetrominoes[Math.floor(Math.random() * tetrominoes.length)]

    const piece = {
      next: generateNewPiece(),
      current: generateNewPiece(),
      pos: { ...startPos },
      state: 0,
    }

    // progress piece

    const progressPiece = () => {
      piece.pos.y += 1
    }

    // move piece

    const movePiece = (direction) => {
      switch (direction) {
        case 'left':
          piece.pos.x -= 1
          break

        case 'right':
          piece.pos.x += 1
          break

        default:
          break
      }
    }

    // rotate piece

    const rotatePiece = () => {
      let nextState = piece.state + 1

      if (nextState > piece.current.rotation.length - 1) nextState = 0

      if (
        checkCollision('left', nextState) ||
        checkCollision('right', nextState)
      )
        nextState = piece.state

      piece.state = nextState
    }

    // set piece

    const setPiece = () => {
      piece.current.rotation[piece.state].forEach((block) => {
        const x = piece.pos.x + block.x
        const y = piece.pos.y + block.y

        board[y][x].occupied = true
      })
    }

    // clear rows
    const clearRow = (row) => {
      board[row].forEach((block) => (block.occupied = false))

      for (let i = row - 1; i >= 0; i--) {
        board[i].forEach(
          (block, j) => (board[i + 1][j].occupied = block.occupied)
        )
      }
    }

    // check rows

    const checkRows = () => {
      let rows = 0

      board.forEach((row, i) => {
        if (row.every((block) => block.occupied)) {
          clearRow(i)
          rows++
        }
      })

      updateScore(rows)
    }

    // create new piece

    const createPiece = () => {
      piece.current = piece.next
      piece.next = generateNewPiece()
      piece.pos = { ...startPos }
      piece.state = 0
    }

    // collision check

    const checkCollision = (direction, state) => {
      let collision = false

      // down
      switch (direction) {
        case 'down':
          piece.current.rotation[state].forEach((block) => {
            if (piece.pos.y + block.y + 1 >= h / bs) {
              collision = true
            } else if (
              board[piece.pos.y + block.y + 1][piece.pos.x + block.x].occupied
            ) {
              collision = true
            }
          })
          break

        // left
        case 'left':
          piece.current.rotation[state].forEach((block) => {
            if (piece.pos.x + block.x <= 0) {
              collision = true
            } else if (
              board[piece.pos.y + block.y][piece.pos.x + block.x - 1].occupied
            ) {
              collision = true
            }
          })
          break

        // right
        case 'right':
          piece.current.rotation[state].forEach((block) => {
            if (piece.pos.x + block.x + 1 >= w / bs) {
              collision = true
            } else if (
              board[piece.pos.y + block.y][piece.pos.x + block.x + 1].occupied
            ) {
              collision = true
            }
          })
          break

        default:
          break
      }

      return collision
    }

    // bind controls

    const bindControls = (e) => {
      switch (e.key) {
        case 'ArrowUp':
          rotatePiece()
          break

        case 'ArrowDown':
          if (!checkCollision('down', piece.state)) progressPiece()
          break

        case 'ArrowLeft':
          if (!checkCollision('left', piece.state)) movePiece('left')
          break

        case 'ArrowRight':
          if (!checkCollision('right', piece.state)) movePiece('right')
          break

        default:
          break
      }
    }

    window.addEventListener('keydown', bindControls)

    // draw

    const ctx = canvas.current.getContext('2d')

    const clear = () => {
      ctx.clearRect(0, 0, w, h)
      nextCtx.clearRect(0, 0, 200, 200)
    }

    const drawBoard = () => {
      ctx.fillStyle = '#316352'

      const occupiedBoard = board.flat().filter((block) => block.occupied)

      occupiedBoard.forEach((block) => {
        const x = block.coords.x * bs
        const y = block.coords.y * bs

        ctx.fillRect(x, y, bs, bs)
      })
    }

    const drawPiece = () => {
      ctx.fillStyle = '#81b66a'
      piece.current.rotation[piece.state].forEach((block) => {
        const x = (piece.pos.x + block.x) * bs
        const y = (piece.pos.y + block.y) * bs

        ctx.fillRect(x, y, bs, bs)
      })
    }

    // draw next

    const nextCtx = next.current.getContext('2d')

    const drawNext = () => {
      nextCtx.fillStyle = '#81b66a'

      piece.next.rotation[0].forEach((block) => {
        const x = block.x * bs
        const y = block.y * bs

        nextCtx.fillRect(x, y, bs, bs)
      })
    }

    // update score

    const updateScore = (lines) => {
      if (lines) {
        setScore((f) => f + lines * lines * 100)
      }
    }

    // tick + tock

    const startTime = 60

    const time = {
      max: startTime,
      current: startTime,
    }

    const tick = () => {
      requestAnimationFrame(tick)

      time.current -= 1

      if (time.current === 0) tock()

      clear()
      drawBoard()
      drawPiece()
      drawNext()
    }

    const tock = () => {
      time.current = time.max

      if (checkCollision('down', piece.state)) {
        setPiece()
        createPiece()
      } else {
        checkRows()
        progressPiece()
      }
    }

    tick()
  }

  // render

  return (
    <div className={c.Tetris}>
      <div className={c.canvasContainer}>
        <div className={c.left}>
          <canvas width={400} height={800} className={c.canvas} ref={canvas} />
        </div>
        <div children={c.right}>
          <h1 className={c.score}>{score}</h1>
          <canvas width={200} height={200} className={c.next} ref={next} />
        </div>
      </div>
      <button onClick={start} className={c.start}>
        Start
      </button>
    </div>
  )
}

export default Tetris
