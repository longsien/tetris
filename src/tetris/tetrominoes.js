// create tetrominoes

const tetrominoes = [
  // I
  {
    name: 'I',
    size: { w: 4, h: 4 },
    rotation: [
      [
        { x: 0, y: 2 },
        { x: 1, y: 2 },
        { x: 2, y: 2 },
        { x: 3, y: 2 },
      ],
      [
        { x: 2, y: 0 },
        { x: 2, y: 1 },
        { x: 2, y: 2 },
        { x: 2, y: 3 },
      ],
    ],
    drawFn: (ctx, pos, block, bs, state, i) => {
      const x = (pos.x + block.x) * bs
      const y = (pos.y + block.y) * bs
      const u = bs / 8

      ctx.fillStyle = '#081820'
      ctx.fillRect(x, y, bs, bs)

      switch (i) {
        case 0:
          state
            ? ctx.clearRect(x + u, y + u, 6 * u, 7 * u)
            : ctx.clearRect(x + u, y + u, 7 * u, 6 * u)
          break

        case 3:
          state
            ? ctx.clearRect(x + u, y, 6 * u, u)
            : ctx.clearRect(x, y + u, u, 6 * u)
          break

        case 1:
          state
            ? ctx.clearRect(x + u, y, 6 * u, 8 * u)
            : ctx.clearRect(x, y + u, 8 * u, 6 * u)
          break

        case 2:
          state
            ? ctx.clearRect(x + u, y, 6 * u, 8 * u)
            : ctx.clearRect(x, y + u, 8 * u, 6 * u)
          break

        default:
          break
      }

      ctx.fillStyle = '#e0f8d0'
      ctx.fillRect(x + u, y + u, 6 * u, 6 * u)

      ctx.fillStyle = '#346856'
      ctx.fillRect(x + u * 5, y + u * 2, 1 * u, 1 * u)

      ctx.fillStyle = '#346856'
      ctx.fillRect(x + u * 6, y + u * 4, 1 * u, 1 * u)

      ctx.fillStyle = '#346856'
      ctx.fillRect(x + u * 3, y + u * 6, 1 * u, 1 * u)

      ctx.fillStyle = '#346856'
      ctx.fillRect(x + u * 5, y + u * 5, 1 * u, 1 * u)
    },
  },
  // O
  {
    name: 'O',
    size: { w: 4, h: 4 },
    rotation: [
      [
        { x: 1, y: 1 },
        { x: 1, y: 2 },
        { x: 2, y: 1 },
        { x: 2, y: 2 },
      ],
    ],
    drawFn: (ctx, pos, block, bs) => {
      const x = (pos.x + block.x) * bs
      const y = (pos.y + block.y) * bs
      const u = bs / 8

      ctx.fillStyle = '#081820'
      ctx.fillRect(x, y, bs, bs)

      ctx.fillStyle = '#e0f8d0'
      ctx.fillRect(x + u, y + u, 6 * u, 6 * u)

      ctx.fillStyle = '#081820'
      ctx.fillRect(x + u * 2, y + u * 2, 4 * u, 4 * u)
    },
  },
  // J
  {
    name: 'J',
    size: { w: 3, h: 3 },
    rotation: [
      [
        { x: 0, y: 1 },
        { x: 1, y: 1 },
        { x: 2, y: 1 },
        { x: 2, y: 2 },
      ],
      [
        { x: 1, y: 0 },
        { x: 1, y: 1 },
        { x: 1, y: 2 },
        { x: 0, y: 2 },
      ],
      [
        { x: 0, y: 0 },
        { x: 0, y: 1 },
        { x: 1, y: 1 },
        { x: 2, y: 1 },
      ],
      [
        { x: 2, y: 0 },
        { x: 1, y: 0 },
        { x: 1, y: 1 },
        { x: 1, y: 2 },
      ],
    ],
    drawFn: (ctx, pos, block, bs) => {
      const x = (pos.x + block.x) * bs
      const y = (pos.y + block.y) * bs
      const u = bs / 8

      ctx.fillStyle = '#081820'
      ctx.fillRect(x, y, bs, bs)

      ctx.fillStyle = '#88c070'
      ctx.fillRect(x + u, y + u, 6 * u, 6 * u)

      ctx.fillStyle = '#081820'
      ctx.fillRect(x + u * 2, y + u * 2, 4 * u, 4 * u)

      ctx.fillStyle = '#e0f8d0'
      ctx.fillRect(x + u * 3, y + u * 3, 2 * u, 2 * u)
    },
  },
  // L
  {
    name: 'L',
    size: { w: 3, h: 3 },
    rotation: [
      [
        { x: 0, y: 1 },
        { x: 1, y: 1 },
        { x: 2, y: 1 },
        { x: 0, y: 2 },
      ],
      [
        { x: 1, y: 0 },
        { x: 1, y: 1 },
        { x: 1, y: 2 },
        { x: 0, y: 0 },
      ],
      [
        { x: 2, y: 0 },
        { x: 0, y: 1 },
        { x: 1, y: 1 },
        { x: 2, y: 1 },
      ],
      [
        { x: 2, y: 2 },
        { x: 1, y: 0 },
        { x: 1, y: 1 },
        { x: 1, y: 2 },
      ],
    ],
    drawFn: (ctx, pos, block, bs) => {
      const x = (pos.x + block.x) * bs
      const y = (pos.y + block.y) * bs
      const u = bs / 8

      ctx.fillStyle = '#081820'
      ctx.fillRect(x, y, bs, bs)

      ctx.fillStyle = '#88c070'
      ctx.fillRect(x + u, y + u, 6 * u, 6 * u)
    },
  },

  // S
  {
    name: 'S',
    size: { w: 3, h: 3 },
    rotation: [
      [
        { x: 1, y: 1 },
        { x: 2, y: 1 },
        { x: 0, y: 2 },
        { x: 1, y: 2 },
      ],
      [
        { x: 0, y: 0 },
        { x: 0, y: 1 },
        { x: 1, y: 1 },
        { x: 1, y: 2 },
      ],
    ],
    drawFn: (ctx, pos, block, bs) => {
      const x = (pos.x + block.x) * bs
      const y = (pos.y + block.y) * bs
      const u = bs / 8

      ctx.fillStyle = '#081820'
      ctx.fillRect(x, y, bs, bs)

      ctx.fillStyle = '#346856'
      ctx.fillRect(x + u, y + u, 6 * u, 6 * u)

      ctx.fillStyle = '#081820'
      ctx.fillRect(x + u * 2, y + u * 2, 4 * u, 4 * u)

      ctx.fillStyle = '#e0f8d0'
      ctx.fillRect(x + u * 3, y + u * 3, 2 * u, 2 * u)
    },
  },

  // Z
  {
    name: 'Z',
    size: { w: 3, h: 3 },
    rotation: [
      [
        { x: 0, y: 1 },
        { x: 1, y: 1 },
        { x: 1, y: 2 },
        { x: 2, y: 2 },
      ],
      [
        { x: 1, y: 0 },
        { x: 0, y: 1 },
        { x: 1, y: 1 },
        { x: 0, y: 2 },
      ],
    ],
    drawFn: (ctx, pos, block, bs) => {
      const x = (pos.x + block.x) * bs
      const y = (pos.y + block.y) * bs
      const u = bs / 8

      ctx.fillStyle = '#081820'
      ctx.fillRect(x, y, bs, bs)

      ctx.fillStyle = '#346856'
      ctx.fillRect(x + u, y + u, 6 * u, 6 * u)

      ctx.fillStyle = '#081820'
      ctx.fillRect(x + u * 3, y + u * 3, 2 * u, 2 * u)
    },
  },

  // T
  {
    name: 'T',
    size: { w: 3, h: 3 },
    rotation: [
      [
        { x: 0, y: 1 },
        { x: 1, y: 1 },
        { x: 2, y: 1 },
        { x: 1, y: 2 },
      ],
      [
        { x: 1, y: 0 },
        { x: 0, y: 1 },
        { x: 1, y: 1 },
        { x: 1, y: 2 },
      ],
      [
        { x: 1, y: 0 },
        { x: 0, y: 1 },
        { x: 1, y: 1 },
        { x: 2, y: 1 },
      ],
      [
        { x: 1, y: 0 },
        { x: 1, y: 1 },
        { x: 2, y: 1 },
        { x: 1, y: 2 },
      ],
    ],
    drawFn: (ctx, pos, block, bs) => {
      const x = (pos.x + block.x) * bs
      const y = (pos.y + block.y) * bs
      const u = bs / 8

      ctx.fillStyle = '#081820'
      ctx.fillRect(x, y, bs, bs)

      ctx.fillStyle = '#88c070'
      ctx.fillRect(x + u, y + u, 6 * u, 6 * u)

      ctx.fillStyle = '#e0f8d0'
      ctx.fillRect(x + u * 2, y + u * 2, 4 * u, 4 * u)

      ctx.fillStyle = '#88c070'
      ctx.fillRect(x + u * 3, y + u * 3, 2 * u, 2 * u)

      ctx.fillStyle = '#081820'
      ctx.fillRect(x + u * 2, y + u * 5, 4 * u, 1 * u)

      ctx.fillStyle = '#081820'
      ctx.fillRect(x + u * 5, y + u * 3, 1 * u, 3 * u)
    },
  },
]

export default tetrominoes
